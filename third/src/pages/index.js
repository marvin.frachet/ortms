import React from "react"
import { graphql } from "gatsby"

const Movies = ({ data }) => (
  <div>
    <h2>Movies</h2>
    {data.allSanityMovie.nodes.map(movie => (
      <article key={movie.title}>
        <h2>{movie.title}</h2>
      </article>
    ))}
  </div>
)

export default Movies

export const query = graphql`
  query MyQuery {
    allSanityMovie {
      nodes {
        title
      }
    }
  }
`
